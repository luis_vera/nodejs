//https://ethereal.email/create
//https://sendgrid.com  //Production
const nodemailer = require("nodemailer");
const sgTransport = require("nodemailer-sendgrid-transport");

let mailConfig;
if (process.env.NODE_ENV==='production') {
  const options = {
    auth:{
      api_key: process.env.SENDGRID_API_SECRET
    }
  }
  mailConfig = sgTransport(options);

}else{
  if (process.env.NODE_ENV==='testing') {
    console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxx');
    const options = {
      auth:{
        api_key: process.env.SENDGRID_API_SECRET
      }
    }
    mailConfig = sgTransport(options);

  }else{

    mailConfig = {
        host: "smtp.ethereal.email",
        port: 587,
        auth: {
          user: process.env.ethereal_user,
          pass: process.env.ethereal_pwd
        }
    }
  }
}

/*
const mailConfig = {
    host: "smtp.ethereal.email",
    port: 587,
    //secure: false, // true for 465, false for other ports
    auth: {
      user: 'clementine89@ethereal.email', // generated ethereal user
      pass: 'seYPHwhXzSanvNzVQu' // generated ethereal password
    }
}
*/

module.exports = nodemailer.createTransport(mailConfig);