var Usuario = require('../models/usuario');

exports.list = function (req, res) {
    Usuario.find({},function(err, usuarios){
        res.render('usuarios/index', {usuarios: usuarios } );
    })
}

exports.update_get = function (req, res) {
    Usuario.findById(req.body.id, function(err,usuario){
        res.render('usuarios/update', {usuario:usuario, errors: err.errors} );
    })
}

exports.update = function (req, res) {
    var update_values = {nombre: req.body.nombre};
    Usuario.findByIdAndUpdate(req.body.id, update_values, function(err,usuario){
        if ( err ) {
            console.log(err);
            res.render('usuarios/update', {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})})
        }else{
            res.redirect('/usuarios');
            return;
        }
    })
}

exports.create_get = function (req, res) {
    res.render('usuarios/create', {errors:{}, usuario: new Usuario() } );
}

exports.create = function (req, res, next) {
    if ( req.body.password != req.body.confirm_password ) {
        res.render('usuarios/create', {errors: {confirm_password:{message:"No coninciden los passwords"}}, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})})
    }

    Usuario.create({nombre: req.body.nombre, email: req.body.email, password: req.body.password}, function(err, nuevoUsuario){
        if ( err ) {
            console.log(err);
            res.render('usuarios/create', {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})})
        }else{
            nuevoUsuario.enviar_email_bienvenida();
            res.redirect('/usuarios');
            return;
        }
    })
}

exports.delete = function (req, res, next) {
    Usuario.findByIdAndDelete(req.body.id, function(err){
        if ( err ) {
            next(err);
        }else{
            res.redirect('/usuarios');
        }
    })
}

