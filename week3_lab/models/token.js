var mongoose = require('mongoose');

const Scheme = mongoose.Schema;
const TokenScheme = new Scheme({
    _userId: {type: mongoose.Schema.Types.ObjectId, required:true, ref: 'Usuario' },
    token: {type: String, required:true },
    createdAt: {type: Date, required:true, default: Date.now, expires:43200 }
})

module.exports = mongoose.model('Token',TokenScheme);